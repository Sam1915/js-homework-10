const buttons = document.querySelectorAll('.btn');
const blocks = document.querySelectorAll('.block');
function connect() {
    let position = getActive();
    blocks.forEach(item => {
        item.classList.remove('active-block');
        if(buttons[position].getAttribute('data-name') === item.id) {
            item.classList.add('active-block');
        }
    });
}

function getActive() {
    for(let i = 0; i < buttons.length; i++) {
        if(buttons[i].classList.contains('active-btn')) {
            return i;
        }
    }
}
buttons.forEach(item => {
    item.addEventListener('click', activeByMouse);
})

function activeByMouse() {
    buttons.forEach(item => {
        item.classList.remove('active-btn');
    });
    this.classList.add('active-btn');
    connect();
}
